﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labaratorochka.Models.Kitchen;
using Labaratorochka.Services.Kitchen;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Labaratorochka.Controllers
{
    public class KitchenController : Controller
    {
        private readonly IKitchenService _kitchenService;

        public KitchenController(IKitchenService kitchenService)
        {
            if (kitchenService == null)
                throw new ArgumentNullException(nameof(kitchenService));

            _kitchenService = kitchenService;
        }


        [HttpGet]
        public IActionResult Index()
        {
            var model = _kitchenService.ProductsList();
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult CreateKitchen()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateKitchen(KitchenCreateModel model)
        {
            try
            {
                _kitchenService.KitchenCreate(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
