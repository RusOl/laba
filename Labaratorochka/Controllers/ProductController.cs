﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labaratorochka.Models.Product;
using Labaratorochka.Services.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Labaratorochka.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            if (productService == null)
                throw new ArgumentNullException(nameof(productService));

            _productService = productService;
        }
        [Authorize]
        [HttpGet]
        public IActionResult Index(int? id, ProductSortAndPageModel model, ProductSortState sortState = ProductSortState.NameAsc)
        {
            if (!id.HasValue)
                throw new ArgumentOutOfRangeException(nameof(id));

            var dishes = _productService.ProductSort(id.Value, model, sortState);
            return View(dishes);
        }
        [Authorize]
        [HttpGet]
        public IActionResult CreateProduct(int? restaurantId)
        {
            if (!restaurantId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(restaurantId));

            var model = _productService.ProductCreateList(restaurantId.Value);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateProduct(ProductCreateModel model)
        {
            try
            {
                _productService.CreateProduct(model);

                return RedirectToAction("Index", "Kitchen");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
