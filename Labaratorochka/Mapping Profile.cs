﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Labaratorochka.DAL.Entities;
using Labaratorochka.Models.Kitchen;
using Labaratorochka.Models.Product;

namespace Labaratorochka
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateModelMap<Kitchen, KitchenModel>();
            CreateModelMap<Kitchen, KitchenCreateModel>();
            CreateModelMap<Product, ProductCreateModel>();
        }

        private void CreateModelMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.KitchenName,
                    src => src.MapFrom(p => p.Kitchen.Name));
        }

        private void CreateProductModelProductToProduct()
        {
            CreateMap<ProductCreateModel, Product>()
                .ForMember(target => target.KitchenId,
                    src => src.MapFrom(p => p.KitchenId));
        }
    }
}
