﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labaratorochka.Models.Kitchen;

namespace Labaratorochka.Services.Kitchen
{
    public interface IKitchenService
    {
        List<KitchenModel> ProductsList();
        void KitchenCreate(KitchenCreateModel model);
    }
}
