﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Labaratorochka.DAL;
using Labaratorochka.Models.Kitchen;

namespace Labaratorochka.Services.Kitchen
{
    public class KitchenService : IKitchenService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public KitchenService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<KitchenModel> ProductsList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var restaurants = unitOfWork.Kitchens.GetAll().ToList();
                return Mapper.Map<List<KitchenModel>>(restaurants);
            }
        }

        public void KitchenCreate(KitchenCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brand = Mapper.Map<DAL.Entities.Kitchen>(model);
                unitOfWork.Kitchens.Create(brand);
            }
        }
    }
}
