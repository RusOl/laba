﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Labaratorochka.DAL;
using Labaratorochka.Models.Kitchen;
using Labaratorochka.Models.Product;

namespace Labaratorochka.Services.Product
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ProductService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public ProductSortAndPageModel ProductSort(int id, ProductSortAndPageModel model, ProductSortState sortState)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var restaurant = unitOfWork.Kitchens.GetById(id);
                KitchenModel restaurantModel = Mapper.Map<KitchenModel>(restaurant);

                var dishes = unitOfWork.Products.GetProductsByKitchenId(id).ToList();
                List<ProductModel> dishModels = Mapper.Map<List<ProductModel>>(dishes);

                var dishSortAndPageModel = dishModels.Sort(sortState);

                int pageSize = 3;
                int count = dishes.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                ProductPageModel dishPageModel = new ProductPageModel(count, page, pageSize);
                dishSortAndPageModel.Products = dishSortAndPageModel.Products.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                dishSortAndPageModel.ProductSortState = sortState;
                dishSortAndPageModel.ProductPageModel = dishPageModel;
                dishSortAndPageModel.Page = page;
                dishSortAndPageModel.KitchenModel = restaurantModel;
                dishSortAndPageModel.KitchenModel.Id = id;

                return dishSortAndPageModel;
            }
        }

        public List<ProductModel> ProductsModelList(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var dishes = unitOfWork.Products.GetProductsByKitchenId(id).ToList();
                List<ProductModel> dishModels = Mapper.Map<List<ProductModel>>(dishes);
                return Mapper.Map<List<ProductModel>>(dishModels);
            }
        }

        public ProductCreateModel ProductCreateList(int kitchenId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var restaurant = unitOfWork.Kitchens.GetById(kitchenId);

                var dishCreateModel = new ProductCreateModel()
                {
                    KitchenId = kitchenId,

                    KitchenModel = Mapper.Map<KitchenModel>(restaurant)
                };

                return dishCreateModel;
            }
        }

        public void CreateProduct(ProductCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var dish = Mapper.Map<DAL.Entities.Product>(model);
                unitOfWork.Products.Create(dish);
            }
        }
    }
}
