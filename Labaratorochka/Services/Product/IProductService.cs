﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labaratorochka.Models.Product;

namespace Labaratorochka.Services.Product
{
    public interface IProductService
    {
        ProductSortAndPageModel ProductSort(int id, ProductSortAndPageModel model, ProductSortState sortState);
        List<ProductModel> ProductsModelList(int id);
        ProductCreateModel ProductCreateList(int kitchenId);
        void CreateProduct(ProductCreateModel model);
    }
}
