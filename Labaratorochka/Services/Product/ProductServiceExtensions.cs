﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labaratorochka.Models.Product;

namespace Labaratorochka.Services.Product
{
    public static class ProductServiceExtensions
    {
        public static ProductSortAndPageModel Sort(this List<ProductModel> productModels, ProductSortState sortState)
        {
            var productSortAndPageModel = new ProductSortAndPageModel()
            {
                NameSort = sortState == ProductSortState.NameAsc ? ProductSortState.NameDesc : ProductSortState.NameAsc,
                ContentSort = sortState == ProductSortState.DescriptionAsc ? ProductSortState.DescriptionDesc : ProductSortState.DescriptionAsc,
                PriceSort = sortState == ProductSortState.PriceAsc ? ProductSortState.PriceDesc : ProductSortState.PriceAsc
            };

            switch (sortState)
            {
                case ProductSortState.NameAsc:
                    productModels = productModels.OrderBy(d => d.Name).ToList();
                    break;
                case ProductSortState.NameDesc:
                    productModels = productModels.OrderByDescending(d => d.Name).ToList();
                    break;
                case ProductSortState.DescriptionAsc:
                    productModels = productModels.OrderBy(d => d.Content).ToList();
                    break;
                case ProductSortState.DescriptionDesc:
                    productModels = productModels.OrderByDescending(d => d.Content).ToList();
                    break;
                case ProductSortState.PriceAsc:
                    productModels = productModels.OrderBy(d => d.Price).ToList();
                    break;
                case ProductSortState.PriceDesc:
                    productModels = productModels.OrderByDescending(d => d.Price).ToList();
                    break;
            }

            productSortAndPageModel.Products = productModels;
            return productSortAndPageModel;
        }
    }
}
