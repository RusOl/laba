﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Labaratorochka.Models.Kitchen
{
    public class KitchenCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Наименование не должно быть пустым")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Описание не должно быть пустым")]
        public string Content { get; set; }
    }
}
