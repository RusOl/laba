﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Labaratorochka.Models.Kitchen
{
    public class KitchenModel
    {
        [Display(Name = "Id:")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Наименование:")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Описание:")]
        [Required]
        public string Content { get; set; }
    }
}
