﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labaratorochka.Models.Kitchen;

namespace Labaratorochka.Models.Product
{
    public class ProductSortAndPageModel
    {
        public KitchenModel KitchenModel { get; set; }

        public ProductModel ProductModel { get; set; }

        public List<ProductModel> Products { get; set; }

        public ProductSortState NameSort { get; set; }

        public ProductSortState ContentSort { get; set; }

        public ProductSortState PriceSort { get; set; }

        public ProductSortState ProductSortState { get; set; }

        public int? Page { get; set; }

        public ProductPageModel ProductPageModel { get; set; }
    }
}
