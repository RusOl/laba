﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labaratorochka.Models.Product
{
    public class ProductPageModel : IEnumerable
    {

        public int PageNumber { get; set; }
        public int TotalPages { get; set; }

        public ProductPageModel(int count, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        }

        public bool HasPreviousPage => PageNumber > 1;

        public bool HasNextPage => PageNumber < TotalPages;
        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
