﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labaratorochka.Models.Product
{
    public enum ProductSortState
    {
        NameAsc,
        NameDesc,
        DescriptionAsc,
        DescriptionDesc,
        PriceAsc,
        PriceDesc
    }
}
