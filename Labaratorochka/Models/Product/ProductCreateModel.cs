﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Labaratorochka.Models.Kitchen;

namespace Labaratorochka.Models.Product
{
    public class ProductCreateModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required]
        public string Content { get; set; }

        [Display(Name = "Кафе")]
        [Required]
        public string KitchenName { get; set; }

        [Display(Name = "Стоимость")]
        [Required]
        public decimal Price { get; set; }
        public int KitchenId { get; set; }
        public KitchenModel KitchenModel { get; set; }
    }
}
