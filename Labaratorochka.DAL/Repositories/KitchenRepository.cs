﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;
using Labaratorochka.DAL.Repositories.Contracts;

namespace Labaratorochka.DAL.Repositories
{
    public class KitchenRepository : Repository<Kitchen>, IKitchenRepository
    {
        public KitchenRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Kitchens;
        }

    }
}
