﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Labaratorochka.DAL.Entities;
using Labaratorochka.DAL.Repositories.Contracts;

namespace Labaratorochka.DAL.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Products;
        }
        public IEnumerable<Product> GetProducts()
        {
            return entities.Include(d => d.Kitchen);
        }
        public IEnumerable<Product> GetProductsByKitchenId(int id)
        {
            return GetProducts().Where(p => p.KitchenId == id);
        }
        public Product AllProductById(int id)
        {
            return GetProducts().FirstOrDefault(d => d.Id == id);
        }
    }
}
