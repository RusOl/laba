﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;

namespace Labaratorochka.DAL.Repositories.Contracts
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> GetProducts();
        IEnumerable<Product> GetProductsByKitchenId(int id);
        public Product AllProductById(int id);
    }
}
