﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;

namespace Labaratorochka.DAL.Repositories.Contracts
{
    public interface IKitchenRepository : IRepository<Kitchen>
    {
    }
}
