﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Repositories;
using Labaratorochka.DAL.Repositories.Contracts;

namespace Labaratorochka.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IKitchenRepository Kitchens { get; set; }
        public IProductRepository Products { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Kitchens = new KitchenRepository(context);
            Products = new ProductRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
