﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Labaratorochka.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public string Basket { get; set; }
    }
}
