﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Labaratorochka.DAL.Entities
{
    public class Kitchen : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
