﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;
using Labaratorochka.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Labaratorochka.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;

        public DbSet<Kitchen> Kitchens { get; set; }
        public DbSet<Product> Products { get; set; }
        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.KitchenConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.ProductConfiguration.ProvideConfigurationAction());
        }
    }
}
