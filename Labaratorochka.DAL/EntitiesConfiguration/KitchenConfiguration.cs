﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Labaratorochka.DAL.EntitiesConfiguration
{
    public class KitchenConfiguration : BaseEntityConfiguration<Kitchen>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Kitchen> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.Content)
                .HasMaxLength(int.MaxValue);
            builder
                .HasIndex(b => b.Name)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Kitchen> builder)
        {
            builder
                .HasMany(b => b.Products)
                .WithOne(b => b.Kitchen)
                .HasForeignKey(b => b.KitchenId)
                .IsRequired();
        }

}
}
