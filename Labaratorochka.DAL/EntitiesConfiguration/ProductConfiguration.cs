﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Labaratorochka.DAL.EntitiesConfiguration
{
    public class ProductConfiguration : BaseEntityConfiguration<Product>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Product> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.Content)
                .HasMaxLength(int.MaxValue);

            builder
                .Property(b => b.Price)
                .HasDefaultValue(0.00M)
                .IsRequired();
            builder
                .HasIndex(b => b.Name)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Product> builder)
        {
            builder
                .HasOne(b => b.Kitchen)
                .WithMany(b => b.Products)
                .HasForeignKey(b => b.KitchenId)
                .IsRequired();
        }

    }
}
