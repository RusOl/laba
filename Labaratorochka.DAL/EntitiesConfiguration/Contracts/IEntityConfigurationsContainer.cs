﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;

namespace Labaratorochka.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Kitchen> KitchenConfiguration { get; }
        IEntityConfiguration<Product> ProductConfiguration { get; }
    }
}
