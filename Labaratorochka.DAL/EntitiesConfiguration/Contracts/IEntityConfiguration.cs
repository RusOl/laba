﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Labaratorochka.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
