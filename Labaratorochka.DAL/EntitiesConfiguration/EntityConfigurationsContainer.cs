﻿using System;
using System.Collections.Generic;
using System.Text;
using Labaratorochka.DAL.Entities;
using Labaratorochka.DAL.EntitiesConfiguration.Contracts;

namespace Labaratorochka.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Kitchen> KitchenConfiguration { get; }
        public IEntityConfiguration<Product> ProductConfiguration { get; }
        public EntityConfigurationsContainer()
        {
            KitchenConfiguration = new KitchenConfiguration();
            ProductConfiguration = new ProductConfiguration();
        }
    }
}
